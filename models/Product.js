const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Product name is required"]
  },
  description: {
    type: String,
    required: [true, "Description is required"]
  },
  price: {
    type: Number,
    required: [true, "Price is required"]
  },
  stocks: {  // Corrected field name from 'slots' to 'stocks'
    type: Number,
    required: [true, "Stocks is required"]  // Corrected error message
  },
  isActive: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    default: Date.now  // Corrected default value
  },
  orders: [
    {
      userId: {
        type: String,
        required: [true, "User Id is required"]
      },
      quantity: {
        type: Number
      },
      datePurchased: {
        type: Date,
        default: Date.now  // Corrected default value
      }
    }
  ]
});

module.exports = mongoose.model("Product", productSchema);
