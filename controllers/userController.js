const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration

module.exports.registerUser = (req, res) => {
  // Check if the email already exists
  User.findOne({ email: req.body.email })
    .then((existingUser) => {
      if (existingUser) {
        // Email already exists, send a response indicating the conflict
        return res.status(409).json({ message: "Email already exists" });
      } else {
        // Email is unique, proceed with user registration
        let newUser = new User({
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          password: bcrypt.hashSync(req.body.password, 10),
          mobileNo: req.body.mobileNo,
        });

        return newUser
          .save()
          .then((user) => {
            console.log(user);
            res.send(true);
          })
          .catch((error) => {
            console.log(error);
            res.send(false);
          });
      }
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({ error: "Failed to register user" });
    });
};

// User Login 

module.exports.userLogin = (req, res) => {

  return User.findOne({email: req.body.email}).then(result => {

    // User does not exist
    if(result == null) {

      return res.send({message: "No user found"});
    // User exists
    } else {

      const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

      if(isPasswordCorrect) {
        // Generate an access token by invoking the "createAccessToken" in the auth.js file
        return res.send({accessToken: auth.createAccessToken(result)});

      // Password do not match
      } else {
        return res.send({message: "Incorrect Password"})
      }
    }
  })
};



// Get User Details

module.exports.getProfile = (req, res) => {
  
  // Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);

  return User.findById(userData.id).then(result => {

    result.password = "";

    // Returns the user information with the password as an empty string
    return res.send(result);
  });
};

// Create an Order

module.exports.createOrder = async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    const product = await Product.findById(req.body.productId);

    if (!product) {
      return res.json({
        message:
          "Product ID does not exist. Stay tuned and keep those shopping dreams alive!",
      });
    }

    const order = {
      products: [
        {
          productId: req.body.productId,
          productName: product.name,
          price: product.price,
          quantity: req.body.quantity,
        },
      ],
      totalAmount: req.body.quantity * product.price,
    };

    console.log(order);

    let user = await User.findById(userData.id);
    console.log(user);

    let isUserUpdated = await User.findByIdAndUpdate(
      userData.id,
      { $push: { orders: order } },
      { new: true }
    );
    console.log(isUserUpdated);

    let isProductUpdated = await Product.findByIdAndUpdate(
      req.body.productId,
      {
        $push: {
          orders: { userId: userData.id, quantity: req.body.quantity },
        },
        $inc: { stocks: -req.body.quantity },
      },
      { new: true }
    );
    console.log(isProductUpdated);

    if (isUserUpdated && isProductUpdated) {
      return res.json({
        success: true,
        message: `Thank you for your order, ${user.firstName}! Sit tight, relax, and get ready for the sweet anticipation of happy mail heading your way.`,
      });
    } else {
      return res.json({ success: false });
    }
  } catch (error) {
    console.log(error);
    return res.json({ success: false });
  }
};



// Set User as Admin

module.exports.setAdmin = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  const setAdmin = {
    isAdmin: req.body.isAdmin
  };

  if (userData.isAdmin) {
    User.findByIdAndUpdate(req.params.userId, setAdmin, { new: true })
      .then((result) => {
        if (setAdmin.isAdmin) {
          console.log(result);
          return User.findById(req.params.userId)
            .then((user) => {
              res.json({
                message: `Congratulations! User ${user.firstName} ${user.lastName} has been granted admin privileges. They are now part of the chosen ones!`,
              });
            })
            .catch((error) => {
              console.log(error);
              res.json(false);
            });
        } else {
          console.log(result);
          return User.findById(req.params.userId)
            .then((user) => {
              res.json({
                message: `You removed ${user.firstName} ${user.lastName} as an admin.`,
              });
            })
            .catch((error) => {
              console.log(error);
              res.json(false);
            });
        }
      })
      .catch((error) => {
        console.log(error);
        res.json(false);
      });
  } else {
    res.json(false);
  }
};
