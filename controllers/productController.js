const Product = require("../models/Product");
const auth = require("../auth");
// Create a new product
/*
	Steps:
		1. Create a conditional statement that will check if the user is an administrator.
		2. Create a new Product object using the mongoose model and the information from the request body
		3. Save the new Product to the database
*/

module.exports.addProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin) {
		// Creates a variable "Product" and instantiates a new "Product" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		});

		// Saves the created object to our database
		return newProduct.save()
		// Product creation is successful
		.then(product => {
			console.log(product);
			res.send(true);
		})
		// Product creation failed
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
	}
	
}

// Retrieve all products
/*
	Step:
	1. Retrieve all the products from the database
*/
module.exports.getAllProduct = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    return Product.find({})
      .then(result => res.json(result))
      .catch(error => res.json({ error: error.message }));
  } else {
    return res.json({ isAdmin: false });
  }
};



// Retrieve all ACTIVE products
/*
	Step:
	1. Retrieve all the products from the database with property of "isActive: true"
*/
module.exports.getAllActive = (req, res) => {

	return Product.find({isActive: true}).then(result => res.send(result));
}


// Retrieving a specific product
/*
	Steps:
	1. Retrieve the product that matches the product ID provided in the URL
*/
module.exports.getProduct = (req, res) => {
  console.log(req.params.productId);

  return Product.findById(req.params.productId)
    .then(result => {
      console.log(result);
      if (result) {
        return res.json(result);
      } else {
        return res.status(404).json({ error: 'Product not found' });
      }
    })
    .catch(error => {
      console.log(error);
      return res.status(500).json({ error: 'Failed to retrieve product' });
    });
};


// Update a product
/*
	Steps:
	1. Create a variable "updatedProduct" which will contain the information retrieved from the request body
	2. Find and update the product using the product ID retrieved from the request params property and the variable "updatedProduct" containing the information from the request body
*/
// Information to update a product will be coming from both the URL parameters and the request body
module.exports.updateProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		}

		// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
		// {new:true} - returns the updated document
		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true})
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
		// return res.send("You don't have access to this page!")
	}
}


// Archive/Unarchive a product
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the product "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active products are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {

		let updateActiveField = {
			isActive : req.body.isActive
		};

		return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
		.then(result => {
			console.log(result);
			// The product status is successfully updated
			return res.send(true)
		})
		.catch(error =>{
			console.log(error);
			// The product update failed
			return res.send(false);
		})

	} else {
		return res.send(false);
	}
	
};

// Deleting a Product

module.exports.deleteProduct = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    return Product.findByIdAndDelete(req.params.productId)
      .then(result => {
        console.log(result);
        // The product is successfully deleted
        const productName = result.name; // Assuming the product name is stored in the 'name' field
        return res.json({
          success: true,
          message: `Product '${productName}' is successfully deleted.`
        });
      })
      .catch(error => {
        console.log(error);
        // The product deletion failed
        return res.json({
          success: false,
          message: "Something went wrong. Please try again"
        });
      });
  } else {
    return res.json({
      success: false
    });
  }
};




// Define the controller functions
// Route for getting products with orders
module.exports.getAllOrders = async (req, res) => {
  try {
    // Retrieve all products with their associated orders
    const productsWithOrders = await Product.find({ orders: { $exists: true, $not: { $size: 0 } } })
      .populate('orders'); // Populate the 'orders' field to retrieve associated orders

    // Example code to send a response
    res.status(200).json({
      message: 'Products with orders retrieved successfully',
      data: productsWithOrders
    });
  } catch (error) {
    console.log('Error retrieving products with orders:', error);
    res.status(500).json({
      message: 'Failed to retrieve products with orders',
      error: error.message
    });
  }
};


