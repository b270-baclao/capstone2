const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth")
const User = require("../models/User");


// Route for user registration
router.post("/register", userController.registerUser);

// Route for user authentication
router.post("/login", userController.userLogin);

// Route that will accept the user’s Id to retrieve the details of a user.
router.get("/details", auth.verify, userController.getProfile);

// Route for creating an order
router.post("/checkOut", auth.verify, userController.createOrder);

// Route for setting a user as admin
router.put("/:userId/setAdmin", auth.verify, userController.setAdmin);

module.exports = router;

